const express = require('express');
const WsServer = require('ws').Server;
const path = require('path');

// http stuff
const app = express();

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

const server = app.listen(8080, () => {
  console.log("started");
});

// websocket stuff
const wsserver = new WsServer({ server });
// const wsserver = new WsServer({ port: 8080 });

// let users_to_sockets = {};

let idToName = {};

const handlers = {
  greeting: (msg) => {
    console.log("got msg", msg.content);
  },
  setNickname: (msg, userId) => {
    idToName[userId] = msg.content;
  }
};

WsServer.prototype.broadcast = function(msg) {
  this.clients.forEach((c) => {
    c.send(JSON.stringify(msg));
  });
};

wsserver.on('connection', (client/*, req*/) => {
  console.log("new client");
  const userId = Math.random();
  idToName[userId] = "Anonymoose";
  client.userId = userId;
  // could use request to do user auth, associate this `client`
  // object/channel/socket with a particular user
  // users_to_sockets[req.session.user_id].append(client);

  // wsserver.clients.forEach((c) => {
  //   c.send("new user joined!")
  // });
  wsserver.broadcast("new user joined!");

  const interval = setInterval(
    () => {
      client.send('beep');
    },
    2500);

  client.on('message', (msgData) => {
    console.log(`message from user ${userId}: ${msgData}`);
    const msg = JSON.parse(msgData);
    handlers[msg.type](msg, userId);
  });

  client.on('close', () => {
    // client has gone away
    clearInterval(interval);
    delete idToName[userId];
    console.log("client left");
  });

  client.send('hello!');

});
