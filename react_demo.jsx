class App extends Component {
  componentDidMount() {
    this.socket = new WebSocket('...');
    this.socket.addEventListener('message', this.gotMsg);
    this.socket.addEventListener('close', this.closed);
    this.socket.addEventListener('open', () => {
      this.setState({closed: false});
    });
  }

  closed() {
    this.setState({closed: true});
  }

  gotMsg(msg) {
    // handle msg
    this.setState((state) => {
      return {msgs: this.state.msgs.concat([msg.data])};
    });
  }

  sendMsg(msg) {
    this.socket.send(JSON.stringify(msg));
  }

  render() {
    return (
      <Child done={this.sendMsg.bind(this)}/>
  }
}
